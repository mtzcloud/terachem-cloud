import pkg_resources
import yaml

# TODO: more sophisticated config management! maybe just put broker URL in yaml?
with pkg_resources.resource_stream(__name__, 'config.yaml') as f:
    config = yaml.load(f)


# Broker settings.
BROKER_URL = "amqp://guest:guest@" + config["rabbitmq"]["host"] + ":" + str(config["rabbitmq"]["port"]) + "//"

# List of modules to import when celery starts.
# CELERY_IMPORTS = ("tasks.py", )

# Use redis to store state/results
CELERY_RESULT_BACKEND = "redis://{host}:{port}".format(
  host=config["redis_celery"]["host"],
  port=config["redis_celery"]["port"]
)

# CELERY_ANNOTATIONS = {"tasks.add": {"rate_limit": "10/s"}}
# Docs say rate limits is complicated code so don't use if you don't need it
CELERY_DISABLE_RATE_LIMITS = True

# acknowledge items in RabbitMQ when a task finishes, not when it begins; this should
# cause tasks to be retried if the worker dies (e.g. after a deploy)
# http://celery.readthedocs.org/en/latest/faq.html#faq-acks-late-vs-retry
CELERY_ACKS_LATE = True

# we manually set a status of SUBMITTED when a job is created by the API server,
# but enabling STARTED also lets us track if/when a worker picks up the job
# http://celery.readthedocs.org/en/latest/userguide/tasks.html#Task.track_started
CELERY_TRACK_STARTED = True

# only fetch one task per core at a time!
# (this assumes CELERYD_CONCURRENCY is set to the number of cores;
# it should default to multiprocessing.cpu_count())
CELERYD_PREFETCH_MULTIPLIER = 1

# actually, only run one task per instance of the worker
# we don't want multiple GPU processes clashing with each other
CELERYD_CONCURRENCY = 1

# disable the default pickle serializer, for forward compatibility
# and to suppress security warnings
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

# define queues
from kombu import Queue, Exchange
# CELERY_ROUTES = {'api.tasks.*': {'queue': 'big'}}
CELERY_QUEUES = (
    Queue('big', Exchange('default'), routing_key='api.tasks.#'),
    # Queue('medium', Excahnge('default'), routing_key='api.tasks.#'),
    # Queue('small', Excahnge('default'), routing_key='api.tasks.#')
)
CELERY_ROUTES = {'api.tasks.*': 'big'}
