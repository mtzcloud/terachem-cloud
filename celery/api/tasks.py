from contextlib import closing
import datetime
import yaml
import json
import logging
from random import randint
import signal
import socket
import subprocess
import time
import os
import numpy as np

from celery import Celery
from celery.app.task import Task

from api.settings import config
from api.data.datahelper import DataHelper
from api.users.limits import LimitManager
from api import job_status as js

from workers.tcpb import TCProtobufClient

celery = Celery('tasks')
celery.config_from_object('api.celeryconfig')

logger = logging.getLogger(__name__)

TCPB_HOST = config['tcpb_server']['host']
TCPB_PORT = config['tcpb_server']['port']

DATA_PATH = config['volumes']['data']
SCRATCH_PATH = config['volumes']['scratch']


class AbInitioTask(Task):
    abstract = True
    ignore_result = False  # Required for chord tasks - e.g. 1d auto

    limit_manager = LimitManager()

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        """ Handle task failure (e.g. task throws an exception) """

        logger.error('--- Task failure ---')
        logger.error(exc)

        # Log failure to REDIS
        user_id = args[0]
        job_id = args[1]
        dh = DataHelper(user_id, job_id)

        dh.set_job_status(js.FAILURE)
        dh.set_job_failure_message(str(exc))

@celery.task(base=AbInitioTask, bind=True, queue='big')
def TerachemJob(self, user_id, job_id, tc_config, geom, enable_profiling=False):
    '''
    Celery wrapper for ab initio single point energy and gradient calculation
    '''
    if enable_profiling:
        logging.info('<== entering celery task {}'.format(datetime.datetime.now().isoformat()))

    logging.info(self.request)

    logging.info(user_id)
    logging.info(job_id)
    logging.info(' '.join([str(k) + ': ' + str(v) + ', ' for k, v in tc_config.iteritems()]))

    # instantiate a data helper object and record where we are running
    dh = DataHelper(user_id, job_id)

    # create a TCPB client instance
    tcClient = TCProtobufClient(TCPB_HOST, TCPB_PORT)
    logging.info("Have host: {} and port: {} for TCProtobufClient".format(TCPB_HOST, TCPB_PORT))

    # if any field references data, make sure to copy that over
    # This relies on config['volumes'] matching across instances
    for k, v in tc_config.iteritems():
        if isinstance(v, basestring) and DATA_PATH in v:
            # Handle multiple paths in one entry (such as unrestricted guesses)
            for v_path in v.split():
                scratch_path = v_path.replace(DATA_PATH, SCRATCH_PATH)

                logging.info('Found reference to {}, copying to local scratch at {}'.format(v_path, scratch_path))
                
                if os.path.isfile(v_path):
                    dh.copy_file(v_path, scratch_path)
                else:
                    dh.copy_dir(v_path, scratch_path)
                tc_config[k] = scratch_path

    # update job status in redis and timestamp it
    dh.set_job_status(js.RUNNING)
    t0 = datetime.datetime.now()
    dh.set_timestamp(str(t0))

    # run terachem calculation
    if enable_profiling:
        logging.info('<== sending job to tcpb server {}'.format(datetime.datetime.now().isoformat()))

    # Remove runtype and units from options, let server handle it
    jobType = tc_config['runtype']
    unitType = tc_config['units']
    del tc_config['runtype']
    del tc_config['units']

    tcClient.connect()
    results = tcClient.compute_job_sync(jobType=jobType, geom=geom, unitType=unitType, **tc_config)
    tcClient.disconnect()

    logging.info('Original results: {}'.format(results))
    for key, value in results.iteritems():
        if isinstance(value, np.ndarray): # Handle NumPy arrays
            results[key] = list(value.flatten())
        elif isinstance(value, list): # Handle list of NumPy arrays
            results[key] = []
            for el in value:
                if isinstance(el, np.ndarray):
                    results[key].append(list(el.flatten()))
                else:
                    results[key].append(el)

    # Store TCPB server job id (different from API server job id)
    dh.set_tc_job_id(results['server_job_id'])
    results['tcpb_job_id'] = results['server_job_id']
    del results['server_job_id']

    # debug
    timestamp = dh.get_timestamp()
    time_taken = dh.get_time_taken()
    logging.info("timestamp of job:  {}".format(timestamp))
    logging.info("time taken by job: {}".format(time_taken))

    # copy scratch results to global file server
    scratch_job_dir = results['job_dir']
    data_job_dir = os.path.join(DATA_PATH, user_id, job_id)
    # Handle trailing slash for consistency in replace later
    if scratch_job_dir[-1] == '/':
        data_job_dir.append('/')

    dh.copy_dir(scratch_job_dir, data_job_dir)
    for k, v in results.iteritems():
        if isinstance(v, basestring) and scratch_job_dir in v:
            results[k] = v.replace(scratch_job_dir, data_job_dir)

    # update job status in redis and record time taken
    t1 = datetime.datetime.now()
    duration = str(t1-t0)

    dh.set_time_taken(duration)

    # TODO: Obviously this breaks limit accounting, but want to get a full run through first
    #self.limit_manager.set_time_taken(duration)
    #self.limit_manager.decrement_job_count(user_id)

    # save results to redis
    logging.info('Final returned results: {}'.format(results))
    results_json = json.dumps(results)
    dh.save_results(results_json)
    dh.set_job_status(js.SUCCESS)
    
    if enable_profiling:
        logging.info('<== leaving celery task {}'.format(datetime.datetime.now().isoformat()))

    return True

