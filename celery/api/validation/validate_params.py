from pkg_resources import resource_stream
import yaml
from collections import OrderedDict

from options import Options


    

class ValidateUserParams(object):

    def __init__(self):
        # load yaml specifications
        with resource_stream(__name__, 'tc_options.yaml') as f:
            self.tc_dict = self.ordered_load(f)

        self.tc_options = Options.load_from_dict(self.tc_dict)

        # TODO: this should probably just be a text file
        with resource_stream(__name__, 'generic.yaml') as f:
            self.generic = yaml.load(f)

    def ordered_load(self, stream):
        """Keep order of YAML file, from https://stackoverflow.com/a/21912744/3052876
        """
        class OrderedLoader(yaml.SafeLoader):
            pass
        def construct_mapping(loader, node):
            loader.flatten_mapping(node)
            return OrderedDict(loader.construct_pairs(node))
        OrderedLoader.add_constructor(
            yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
            construct_mapping)
        return yaml.load(stream, OrderedLoader)

    def documentation(self, engine=None):
        if engine is None:
            return self.generic
        elif engine.lower() == 'terachem':
            return self.tc_options

    def validate_params(self, engine, user_params):

        trial_options = self.documentation(engine).copy()
        
        errors = {}

        # check all required keys are here
        required_options = [k for k,v in trial_options.options.items() if v.required]
        for ro in required_options:
            if ro not in user_params.keys():
                errors[ro] = "Required keyword not provided"

        # check values are allowed
        for k, v in user_params.iteritems():
            try:
                trial_options[k] = v
            except (RuntimeError, ValueError) as e:
                errors[k] = e.message

        return errors
