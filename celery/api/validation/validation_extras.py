

def validate_user(user_id):
    # TODO: criteria?
    return None


def validate_geometry(geom, config):
    # check that geom describes a molecule we can feed to TeraChem
    # TODO: Complete
    if len(geom) != 3*len(config['atoms']):
        return "Geometry does not match atoms from configuration"

    return None


def decide_queue(config, geom):
    """Heuristic for assigning tasks to queue
    """
    # for the moment just on molecule size
    # TODO: include methods
    if len(config['atoms']) < 30:
        q = 'small'
    elif len(config['atoms']) > 100:
        q = 'big'
    else:
        q = 'medium'
    return q
