import pkg_resources
import yaml

# Read settings from config file
with pkg_resources.resource_stream(__name__, 'config.yaml') as f:
    config = yaml.load(f)

###############################################################################
# Tornado / API server execution settings
# These are the defaults, which can be overridden by command-line flags

TORNADO = {
    "debug": False,
    "autoreload": False,
    "use_authgami": True,
    "enable_throttling": True,
    "enable_monitoring": True
}

###############################################################################

