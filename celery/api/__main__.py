# Terachem API module
# Execute with python -m api from the directory above
import logging

import tornado.ioloop
import tornado.web
import tornado.autoreload
from tornado.options import define, options

from api import RequestHandlers as rh
from api.users import UserManager, LimitManager

from validation.validate_params import ValidateUserParams

rh_kwargs = {
    'params_validator': ValidateUserParams(),
    'user_manager': UserManager(),
    'limits_manager': LimitManager()
}


def get_app(settings=None):

    if settings is None:
        settings = {}

    application = tornado.web.Application([

        # admin
        (r"/admin", rh.AdminHandler, rh_kwargs),

        # login handling, should be first point of contact by client
        (r"/login", rh.LoginHandler, rh_kwargs),

        # receive geometry, return energy and forces
        (r"/v1/terachem/?", rh.TerachemRH, rh_kwargs),
        (r"/v1/lightspeed/?", rh.LightspeedRH, rh_kwargs),

        # documentation
        (r"/v1/docs/?", rh.DocumentationRH, rh_kwargs),

        # retrieve results
        (r"/v1/job/?", rh.ResultDetailsRH, rh_kwargs),

        # Root route
        (r"/(?:v1)?/?", rh.RootRH),

        # Custom 404 error page
        (r".*", rh.MissingRH)


    ], settings)

    application.sentry_client = None

    return application


define(
    "dev",
    default=True,
    help=(
        "development mode: set to False disable. When enabled, Tornado will "
        "auto-reload on file change and will be in debug mode, and "
        "external monitoring will be disabled"
    )
)
define(
    "enable_monitoring",
    default=None,    # only override --dev if set to 1 or 0
    help="enable or disable reporting to external monitoring tools; overrides --dev"
)
define(
    "enable_profiling",
    default=False,
    help="Turn on time stamps in various places to aid in performance tuning"
)


def main():

    define("port", default=8888, help="listen on this port for HTTP connections")
    options.parse_command_line()

    settings = {}

    if options.dev:
        logging.info("Starting in debug mode")
        settings["debug"] = True
        settings["autoreload"] = True
        settings["enable_monitoring"] = False

    if options.enable_profiling:
        logging.info('profiling enabled')
        settings["profile"] = True

    application = get_app(settings=settings)

    if settings["autoreload"]:
        tornado.autoreload.start()

    application.listen(options.port)
    logging.info("Starting tornado server on port {}".format(options.port))
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
