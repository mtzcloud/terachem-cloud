import logging
import redis
from redis.exceptions import ConnectionError
from api.settings import config
from api.exceptions import APIException

logger = logging.getLogger(__name__)


class RedisHelper(object):

    def __init__(self, database_number=0):

        try:
            self.conn = redis.StrictRedis(
                host=config["redis_api"]["host"],
                port=config["redis_api"]["port"],
                db=database_number
            )
            self.conn.ping()
        except ConnectionError as e:
            logging.info(e)
            raise APIException('Cannot connect to Redis: {}'.format(e))

        self.expiry = config["redis_api"]["expiry"]

    def get(self, key):
        return self.conn.get(key)

    def set(self, key, value):
        return self.conn.set(key, value)

    def setex(self, key, value, timeout=None):
        if timeout is None:
            timeout = self.expiry
        return self.conn.setex(key, timeout, value)

    def exists(self, key):
        return self.conn.exists(key)

    def delete(self, key):
        return self.conn.delete(key)

    def keys(self, pattern):
        return self.conn.keys(pattern)
