import json
import logging

from .redishelper import RedisHelper
from .nashelper import NASHelper

logger = logging.getLogger(__name__)


class DataHelper(object):
    """
    Parent for data handling, eg Redis, NAS and whoever else
    """

    def __init__(self, user_id, job_id):

        self.user_id = user_id
        self.job_id = job_id

        self.nas = NASHelper(user_id, job_id)
        self.redis = RedisHelper()

    # results in redis
    def save_results(self, value, expire_seconds=None):
        # NOTE: value should be a string, expire should be number of seconds
        key = 'results:{user_id}:{job_id}'.format(job_id=self.job_id, user_id=self.user_id)
        logging.info("KEY: {} VALUE (type: {}): {}".format(key, type(value), value))
        self.redis.setex(key, value, expire_seconds)

    def get_results(self):
        key = 'results:{user_id}:{job_id}'.format(job_id=self.job_id, user_id=self.user_id)
        value = self.redis.get(key)
        return value

    def check_results_exist(self):
        key = 'results:{user_id}:{job_id}'.format(job_id=self.job_id, user_id=self.user_id)
        return self.redis.exists(key)

    def delete_results(self):
        """ Delete a job - used in testing, not exposed to API yet """
        key = 'results:{user_id}:{job_id}'.format(job_id=self.job_id, user_id=self.user_id)
        deleted = self.redis.delete(key)
        return deleted

    # Job details in redis

    def set_job_status(self, value):
        key = 'job:{user_id}:{job_id}'.format(job_id=self.job_id, user_id=self.user_id)
        self.redis.set(key, value)

    def get_job_status(self):
        key = 'job:{user_id}:{job_id}'.format(job_id=self.job_id, user_id=self.user_id)
        value = self.redis.get(key)
        return value

    def set_job_failure_message(self, value):
        key = 'job:{user_id}:{job_id}:failure'.format(job_id=self.job_id, user_id=self.user_id)
        self.redis.set(key, value)

    def get_job_failure_message(self):
        key = 'job:{user_id}:{job_id}:failure'.format(job_id=self.job_id, user_id=self.user_id)
        value = self.redis.get(key)
        return value

    def check_job_exists(self):
        key = 'job:{user_id}:{job_id}'.format(job_id=self.job_id, user_id=self.user_id)
        return self.redis.exists(key)

    def delete_job(self):
        """ Delete a job - used in testing, not exposed to API yet """
        key = 'job:{user_id}:{job_id}'.format(job_id=self.job_id, user_id=self.user_id)
        deleted = self.redis.delete(key)
        return deleted
    
    # Celery job id (for info)

    def set_celery_job_id(self, value):
        key = 'celery_job_id:{user_id}:{job_id}'.format(job_id=self.job_id, user_id=self.user_id)
        self.redis.set(key, value)

    def get_celery_job_id(self):
        key = 'celery_job_id:{user_id}:{job_id}'.format(job_id=self.job_id, user_id=self.user_id)
        value = self.redis.get(key)
        return value

    def check_celery_job_id_exists(self):
        key = 'celery_job_id:{user_id}:{job_id}'.format(job_id=self.job_id, user_id=self.user_id)
        return self.redis.exists(key)

    # TeraChem server job details in Redis

    def set_tc_job_id(self, value):
        key = 'tc_job_id:{user_id}:{job_id}'.format(job_id=self.job_id, user_id=self.user_id)
        self.redis.set(key, value)

    def get_tc_job_id(self):
        key = 'tc_job_id:{user_id}:{job_id}'.format(job_id=self.job_id, user_id=self.user_id)
        value = self.redis.get(key)
        return value

    def check_tc_job_id_exists(self):
        key = 'tc_job_id:{user_id}:{job_id}'.format(job_id=self.job_id, user_id=self.user_id)
        return self.redis.exists(key)

    # Timing Data in Redis

    def get_timestamp(self):
        key = 'job:{user_id}:{job_id}:timestamp'.format(job_id=self.job_id, user_id=self.user_id)
        value = self.redis.get(key)
        return value

    def get_time_taken(self):
        key = 'job:{user_id}:{job_id}:time_taken'.format(job_id=self.job_id, user_id=self.user_id)
        value = self.redis.get(key)
        return value

    def set_timestamp(self, value):
        key = 'job:{user_id}:{job_id}:timestamp'.format(job_id=self.job_id, user_id=self.user_id)
        self.redis.set(key, value)

    def set_time_taken(self, value):
        key = 'job:{user_id}:{job_id}:time_taken'.format(job_id=self.job_id, user_id=self.user_id)
        self.redis.set(key, value)

    # results to and from NAS

    def get_result(self, filename='results.json'):
        """ Get data from the NAS return a python object"""
        obj_str = self.nas.get_file(filename)
        logging.info('obj_str=')
        logging.info(obj_str)
        if obj_str is not None:
            if filename.split('.')[-1] == 'json':
                res = json.loads(obj_str)
                logging.info('json.loads(obj_str)=')
                logging.info(res)
            else:
                res = obj_str
        else:
            res = ""
        return res

    def delete_result(self, filename='results.json'):
        """ Delete result - used in testing, not exposed to API yet """
        return self.nas.delete(filename)

    def save_result(self, obj, filename='results.json'):
        """Write a python object to the NAS """
        obj_str = json.dumps(obj)
        value = self.nas.save_file(obj_str, filename)
        return value

    # copy utilities for moving around scratch files after terachem jobs
    def copy_dir(self, origin, destination):
        value = self.nas.copy_dir(origin, destination)
        return value

    def copy_file(self, origin, destination):
        value = self.nas.copy_file(origin, destination)
        return value
