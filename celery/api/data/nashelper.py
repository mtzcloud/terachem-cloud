import logging
import os
import shutil
from api.settings import config

logger = logging.getLogger(__name__)


class NASHelper(object):

    PATH = config['volumes']['data']

    def __init__(self, user_id, job_id, debug=False):
        self.user_id = user_id
        self.job_id = job_id

        if debug:
            self.PATH = "/tmp/NAS/tccloud"

    def make_path(self, job_id, user_id, filename):
        path = os.path.join(self.PATH, str(user_id), str(job_id), filename)
        return path

    def get_file(self, filename):
        """ Check if the file exists on the NAS """
        if os.path.split(filename)[0] == []:
            file_path = self.make_path(self.job_id, self.user_id, filename)
        else:
            file_path = filename
        if os.path.isfile(file_path):
            with open(file_path) as f:
                data = f.read()
            return data
        else:
            return None

    def delete_file(self, job_id, user_id, filename):
        """ Delete file """
        if os.path.split(filename)[0] == []:
            file_path = self.make_path(self.job_id, self.user_id, filename)
        else:
            file_path = filename
        if os.path.isfile(file_path):
            try:
                os.remove(file_path)
                return True
            except OSError as e:
                logging.warn("Cannot delete {} from NAS. Error is: {}".format(file_path, e))
                raise
        else:
            return False

    def save_file(self, job_id, user_id, string, filename):
        """ Write string  """
        if os.path.split(filename)[0] == []:
            file_path = self.make_path(self.job_id, self.user_id, filename)
        else:
            file_path = filename
        with open(file_path, 'wb') as f:
            try:
                f.write(string)
            except IOError as e:
                logging.warn("Error writing {} to NAS: {}".format(file_path, e))
                raise

        return True

    def copy_file(self, origin_path, dest_path):
        """ Copy file """
        if os.path.isfile(dest_path):
            logging.info("{} already exists, skipping copy".format(dest_path))
            return True

        #Preemptively try to make parent directory
        try:
            dest_dir = os.path.dirname(dest_path)
            os.makedirs(dest_dir)
        except (IOError, OSError) as e:
            logging.warn("Error creating parent directories for file {}: {}".format(dest_path, e))
            pass

        try:
            shutil.copyfile(origin_path, dest_path)
        except (IOError, OSError) as e:
            logging.warn("Error copying {} to {}: {}".format(origin_path, dest_path, e))
            raise

        return True

    def copy_dir(self, origin_path, dest_path):
        """ Copy directory """
        if os.path.isdir(dest_path):
            logging.info("{} already exists, skipping copy".format(dest_path))
            return True

        try:
            shutil.copytree(origin_path, dest_path)
        except (IOError, OSError) as e:
            logging.warn("Error copying {} to {}: {}".format(origin_path, dest_path, e))
            raise

        return True

