import logging
import json

from tornado.web import RequestHandler

from api import status

class AdminHandler(RequestHandler):
    """
    Class to handle admin requests, mostly user creation and limit management
    """
    SUPPORTED_METHODS = ('POST')
    _SECRET = 'a13aa57811ce4c7823dbbf3542ba5f708969fefe7fd403b3eab262007ddbb314'

    def initialize(self, params_validator, user_manager, limits_manager):
        self.user_manager = user_manager
        self.limits_manager = limits_manager
        logging.info('created AdminHandler')

    def prepare(self):
        """
        This endpoint is intended solely for API access with a JSON request body
        """
        if self.request.headers["Content-Type"].startswith("application/json"):
            self.json_args = json.loads(self.request.body)
        else:
            self.json_args = None
        # logging.info(self.json_args)

    def post(self):
        """
        Allows to get the list of current users and their limits
        """
        logging.info('inside post method: ' + self.request.body)

        # very weak security
        if self.json_args['user_id'] == 'admin' and self.json_args['magic_word'] == self._SECRET:
            logging.info('passed weakest security ever')
        else:
            response = {'message': 'These are not the credentials you are looking for'}
            self.set_status(status.HTTP_401_UNAUTHORIZED)
            self.write(response)
            return

        # simple server is alive response
        if self.json_args['action'] == 'ping':
            response = {'message': "pong"}
            self.set_status(status.HTTP_200_OK)
            self.write(response)
            return

        elif self.json_args['action'] == 'create_user':
            target_user = self.json_args['target_user']
            # check if user already exists
            if self.user_manager.user_exists(target_user):
                response = {'message': "Failed to create user '{}' because they already exist!".format(target_user)}
                self.set_status(status.HTTP_400_BAD_REQUEST)
            else:
                self.user_manager.create_user(target_user)
                api_key = self.user_manager.generate_api_key(target_user)
                self.limits_manager.set_limits(target_user)
                response = {
                    'message': "user '{}' created".format(target_user),
                    'API_KEY': api_key
                }
                self.set_status(status.HTTP_200_OK)
            self.write(response)
            return

        elif self.json_args['action'] == 'delete_user':
            target_user = self.json_args['target_user']
            success = self.user_manager.delete_user(target_user)
            if success:
                response = {'message': "user '{}' deleted".format(target_user)}
                self.set_status(status.HTTP_200_OK)
            else:
                response = {'message': "Failed to delete user '{}' because they don't exist!".format(target_user)}
                self.set_status(status.HTTP_400_BAD_REQUEST)
            self.write(response)
            return

        # get a single user
        elif self.json_args['action'] == 'list_user':
            target_user = self.json_args['target_user']
            limits = self.limits_manager.get_user_limits(target_user)
            usage = self.limits_manager.get_user_consumption(target_user)
            api_key = self.user_manager.get_api_key(target_user)
            response = {
                'message': "user '{}' details retrieved".format(target_user),
                'user': target_user,
                'limits': limits,
                'usage': usage,
                'api_key': api_key
            }
            self.set_status(status.HTTP_200_OK)
            self.write(response)
            return

        # get a dump
        elif self.json_args['action'] == 'list_users':
            users = self.user_manager.list_all_users()
            if users is not None:
                response = {
                    'message': "Details of all known users retrieved"
                }
                for user in users:
                    limits = self.limits_manager.get_user_limits(user)
                    usage = self.limits_manager.get_user_consumption(user)
                    response[user] = {'limits': limits, 'usage': usage}
            else:
                response = {'message': "No users found"}

            self.set_status(status.HTTP_200_OK)
            self.write(response)
            return

        elif self.json_args['action'] == 'change_limit':
            target_user = self.json_args['target_user']
            success = self.limits_manager.set_limits(target_user)
            return

        elif self.json_args['action'] == 'new_api_key':
            target_user = self.json_args['target_user']
            api_key = self.user_manager.generate_api_key(target_user)
            response = {
                'message': 'generated new API_KEY for user {}'.format(target_user),
                'API_KEY': api_key
            }
            self.set_status(status.HTTP_200_OK)
            self.write(response)
            return

