import logging
import json

from tornado.web import RequestHandler

from api import status
from base import BaseRH

class LoginHandler(BaseRH):
    """
    First point of contact by API client
    client needs to supply a user name and an API key
    """
    SUPPORTED_METHODS = ('POST')

    def post(self):

        # unpack the parsed json body from the POST message
        user_id = self.json_args.get('user_id', None)
        api_key = self.json_args.get('api_key', None)

        logging.info(self.json_args)

        # see if user is in the database
        auth, reason = self.authenticate_user(user_id, api_key)
        
        if not auth:
            self.set_status(status.HTTP_401_UNAUTHORIZED)
        else:
            self.set_status(status.HTTP_200_OK)

        response = {'message': reason}
        self.write(response)
        return
