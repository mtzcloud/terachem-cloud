import logging
import json
import uuid
import datetime

from api.data.datahelper import DataHelper
from api import status
from api import job_status as js
from api import tasks
from api.validation.validation_extras import validate_geometry, decide_queue
from base import BaseRH

logger = logging.getLogger(__name__)


class TerachemRH(BaseRH):
    """
    Class to handle Terachem requests
    Takes a config object (dict in json) and a geometry,
    returns electronic structure data
    """

    SUPPORTED_METHODS = ('POST')

    def post(self):

        if self.options['enable_profiling']:
            logging.info('<== entered request handler {}'.format(datetime.datetime.now().isoformat()))

        # Bugfix for backwards compatiblity with pytc<=1.5.1
        if 'tc_config' in self.json_args.keys():
            self.json_args['config'] = self.json_args['tc_config']

        # Sanity check
        required_keys = ['user_id', 'api_key', 'config', 'geom']
        valid, reason = self.check_json_keys(required_keys)
        if not valid:
            response = {'message': reason}
            self.set_status(status.HTTP_400_BAD_REQUEST)
            self.write(response)
            return

        # unpack the parsed json body from the POST message
        user_id = self.json_args['user_id']
        api_key = self.json_args['api_key']
        config = self.json_args['config']
        geom = self.json_args['geom']

        # validate user
        auth, reason = self.authenticate_user(user_id, api_key)
        if not auth:
            response = {'message': reason}
            self.set_status(status.HTTP_401_UNAUTHORIZED)
            self.write(response)
            return

        # validate terachem parameters
        errors = self.params_validator.validate_params('terachem', config)
        if errors:
            response = {'message': 'Invalid job configuration', 'errors': errors}
            self.set_status(status.HTTP_400_BAD_REQUEST)
            self.write(response)
            return

        logging.info('validated config: {}'.format(config))

        # validate geometry
        error = validate_geometry(geom, config)
        if error is not None:
            response = {'message': error}
            self.set_status(status.HTTP_400_BAD_REQUEST)
            self.write(response)
            return
        logging.info('validated geom: {}'.format(geom))

        # validate resource limits
        usage, reason = self.check_limits(user_id)
        if not usage:
            pass

        # We passed validation so create a job_id and store it
        job_id = str(uuid.uuid4())

        # log job id to redis
        dh = DataHelper(user_id, job_id)
        dh.set_job_status(js.RECEIVED)
        logging.debug("job status in redis: {}".format(dh.get_job_status()))

        # TODO: decide which queue the job should go to
        # Also take into account user limits here
        # queue = decide_queue(config, geom)

        # TODO: 
        if self.options['enable_profiling']:
            logging.info('<== Submitting job to celery {}'.format(datetime.datetime.now().isoformat()))

        # submit celery job
        job_submit = tasks.TerachemJob.apply_async(args=[user_id, job_id, config, geom, self.options['enable_profiling']])
        dh.set_celery_job_id(job_submit)

        logging.info('Celery returned: {}'.format(job_submit))

        # log to redis
        dh.set_job_status(js.SUBMITTED)
        logging.debug("job status in redis: {}".format(dh.get_job_status()))

        # info to be returned to the user
        response = {'message': 'job submitted', 'job_id': job_id, 'job_status': dh.get_job_status()}

        self.set_status(status.HTTP_200_OK)
        self.write(response)

        if self.options['enable_profiling']:
            logging.info('<== leaving request handler {}'.format(datetime.datetime.now().isoformat()))
