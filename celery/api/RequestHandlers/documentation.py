import datetime
import logging
import json

from tornado.web import RequestHandler
from tornado.options import options

from api import status
from base import BaseRH

logger = logging.getLogger(__name__)


class DocumentationRH(BaseRH):
    """
    Class for returning engine documentation.

    """

    SUPPORTED_METHODS = ('POST', 'GET')

    def post(self):
        """POST method has 1 mandatory arguments in body:
            engine (str): Target engine/backend for documentation

        POST returns a dictionary of engine options
        """

        if self.options['enable_profiling']:
            logging.info('<== entered request handler {}'.format(datetime.datetime.now().isoformat()))

        # unpack the parsed json body from the POST message
        if self.json_args is None:
            docs = self.params_validator.documentation()

        else:
            engine = self.json_args['engine']
            logging.info('engine: {}'.format(engine))
            options = self.params_validator.documentation(engine)
            docs = {'docs': str(options)}

        self.set_status(status.HTTP_200_OK)
        self.write(docs)

        if self.options['enable_profiling']:
            logging.info('<== leaving request handler {}'.format(datetime.datetime.now().isoformat()))

    def get(self):
        """Functions exactly like POST
        """
        self.post()

# NOTE: can be tested from the command line with (careful with the quoting of the json body)
# curl -H "Content-Type: application/json" -X POST -d '{"engine":"terachem"}' localhost/v1/docs
