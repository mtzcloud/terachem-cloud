import logging
import json
from tornado.web import RequestHandler

from api.data.datahelper import DataHelper
from api import status
from api import tasks
from api.validation.validation_extras import validate_geometry, validate_user, decide_queue
from base import BaseRH

logger = logging.getLogger(__name__)


class LightspeedRH(BaseRH):
    """
    Class to handle lightspeed calculations
    Takes an ls_config object (dict in json) and a geometry,
    returns electronic structure data
    """

    SUPPORTED_METHODS = ('POST')

    def post(self):
        self.set_status(status.HTTP_200_OK)
        self.write({'message': 'TODO: LightSpeed endpoint'})

