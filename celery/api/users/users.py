import os
import logging
import datetime
from base64 import b64encode
from api.data import RedisHelper


class UserManager(object):
    """
    Class to manage the creation and deletion of user accounts,
    and management of their API keys
    """

    def __init__(self):

        self.redis = RedisHelper(database_number=1)

    def validate_string(self, label):
        """Ensure passed in string is acceptable (currently just a valid basestring)
        """
        if not isinstance(label, basestring):
            raise ValueError('{} is not a valid string'.format(label))

    def validate_int(self, number):
        if not number.is_digit():
            raise ValueError('{} is not a valid integer'.format(number))

    # ------------------------------------------------------------
    #  user creation etc
    # ------------------------------------------------------------
    def create_user(self, user_name):
        """Create a user
        """
        self.validate_string(user_name)
        timestamp = datetime.datetime.now().isoformat()
        key = 'user_created:{}'.format(user_name)
        self.redis.set(key, timestamp)

    def delete_user(self, user_name):
        """Delete a user
        """
        self.validate_string(user_name)
        key = 'user_created:{}'.format(user_name)
        result = self.redis.delete(key)
        if result == 0:
            return False
        elif result == 1:
            return True

    def user_exists(self, user_name):
        """Check a user exists, if they do return the datestamp of their creation
        """
        self.validate_string(user_name)
        key = 'user_created:{}'.format(user_name)
        return self.redis.get(key)

    def list_all_users(self):
        pattern = 'user_created:*'
        result = self.redis.keys(pattern)
        if result is not None:
            user_names = [a.split(':')[1] for a in result]
            creation_times = [self.redis.get(k) for k in result]
            return dict(zip(user_names, creation_times))

    # ------------------------------------------------------------
    #  user auth
    # ------------------------------------------------------------
    def generate_api_key(self, user_name):
        self.validate_string(user_name)
        api_key = b64encode(os.urandom(32))
        # api_key = bcrypt.hashpw(a, bcrypt.gensalt())
        key = 'user_key:{}'.format(user_name)
        self.redis.set(key, api_key)
        return api_key

    def get_api_key(self, user_name):
        self.validate_string(user_name)
        key = 'user_key:{}'.format(user_name)
        return self.redis.get(key)

    # TODO: this should be more robust
    def validate_api_key(self, user_name, supplied_key):
        real_key = self.get_api_key(user_name)
        return real_key == supplied_key

