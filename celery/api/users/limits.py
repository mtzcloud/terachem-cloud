from api.data import RedisHelper


class LimitManager(object):
    """
    Class to manage resource limits

    We impose three kinds of limits:
        MAX_JOBS - the number of simultaneous jobs in the queues
        MAX_RATE - the number of jobs submitted in the last 24 hours
        MAX_USAGE - the total number of resource hours (reset quarterly?)

    """

    def __init__(self):

        self.redis = RedisHelper(database_number=2)

        # limits
        self.MAX_JOBS = 4           # number of simultaneous jobs allowed in the queue
        self.MAX_RATE = 100000      # number of jobs allowed per 24 hours
        self.MAX_USAGE = 10000      # total resource usage = sum_i job_i*time_i

    def validate_string(self, label):
        """Ensure passed in string is acceptable (currently just a valid basestring)
        """
        if not isinstance(label, basestring):
            raise ValueError('{} is not a valid string'.format(label))

    def validate_int(self, number):
        if type(number) is not int and not number.is_digit():
            raise ValueError('{} is not a valid integer'.format(number))

    # ------------------------------------------------------------
    #  user limit management
    # ------------------------------------------------------------

    def set_user_job_limit(self, user_name, limit):
        self.validate_string(user_name)
        self.validate_int(limit)
        key = 'user_job_limit:{}'.format(user_name)
        self.redis.set(key, limit)

    def set_user_job_rate_limit(self, user_name, rate):
        self.validate_string(user_name)
        self.validate_int(rate)
        key = 'user_rate_limit:{}'.format(user_name)
        self.redis.set(key, rate)

    def set_user_usage_limit(self, user_name, usage):
        self.validate_string(user_name)
        self.validate_int(usage)
        key = 'user_usage_limit:{}'.format(user_name)
        self.redis.set(key, usage)

    def set_limits(self, user_name, **kwargs):
        self.validate_string(user_name)
        self.set_user_job_limit(user_name, self.MAX_JOBS)
        self.set_user_job_rate_limit(user_name, self.MAX_RATE)
        self.set_user_usage_limit(user_name, self.MAX_USAGE)

    def get_user_job_limit(self, user_name):
        self.validate_string(user_name)
        key = 'user_job_limit:{}'.format(user_name)
        return self.redis.get(key)

    def get_user_job_rate_limit(self, user_name):
        self.validate_string(user_name)
        key = 'user_rate_limit:{}'.format(user_name)
        return self.redis.get(key)

    def get_user_usage_limit(self, user_name):
        self.validate_string(user_name)
        key = 'user_usage_limit:{}'.format(user_name)
        return self.redis.get(key)

    def get_user_limits(self, user_name):
        # convenience wrapper
        limits = {
            'job_limit': self.get_user_job_limit(user_name),
            'rate_limit': self.get_user_job_rate_limit(user_name),
            'usage_limit': self.get_user_usage_limit(user_name)
        }
        return limits

    # ------------------------------------------------------------
    #  user accounting
    # ------------------------------------------------------------

    # workflow as follows:
    # 1. when request received, check user is within limits
    # 2. when task submitted, timestamp job start
    # 3. when task completed, record task duration
    #    NOTE: we set all task durations to have a 24 hour expiry, rate calculation is then just all tasks
    # 4. update total usage
    #    TODO: weight usage by the queue so that 8 GPU jobs count more than 2 GPU jobs

    def get_user_job_count(self, user_name):
        """Returns the number of currently running jobs owned by user_name
        """
        self.validate_string(user_name)
        key = 'user_job_count:{}'.format(user_name)
        return self.redis.get(key)

    def increment_user_job_count(self, user_name):
        self.validate_string(user_name)
        key = 'total_job_count:{}'.format(user_name)
        self.redis.conn.incr(key)

    def decrement_user_job_count(self, user_name):
        self.validate_string(user_name)
        key = 'total_job_count:{}'.format(user_name)
        self.redis.conn.decr(key)

    def calculate_job_rate(self, user_name):
        self.validate_string(user_name)
        key = 'total_job_count:{}'.format(user_name)

    # def calculate_usage(self, user_name):
    #     self.validate_string(user_name)
    #     key = 'user_usage:{}'.format(user_name)

    def get_user_consumption(self, user_name):
        # convenience wrapper
        consumption = {
            'job_count': self.get_user_job_count(user_name),
            # 'job_rate': self.calculate_job_rate(user_name),
            # 'usage': self.calculate_usage(user_name)
        }
        return consumption
