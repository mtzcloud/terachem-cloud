#!/bin/bash
VERSION=0.2

docker build -t drenerbas/mtzgroup:public-api-v$VERSION .
docker push drenerbas/mtzgroup:public-api-v$VERSION

docker run -it -p 80:8888 drenerbas/mtzgroup:public-api-v$VERSION 

