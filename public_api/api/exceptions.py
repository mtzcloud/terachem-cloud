from api import status
from api import job_status


def force_text(thing):
    # Coerce to unicode
    # Maybe a better way to do this?
    return unicode(thing)


###############################################################################
#                                                                             #
#                             Generic API Errors                              #
#                                                                             #
###############################################################################
class APIException(Exception):
    """
    Base class for API exceptions.
    Subclasses should provide `.status_code` and `.default_detail` properties.
    """
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    default_detail = 'A server error occured'

    def __init__(self, detail=None):
        if detail is not None:
            self.detail = force_text(detail)
        else:
            self.detail = force_text(self.default_detail)

    # def __str__(self):
    #     return "<{type}: '{msg}'>".format(
    #         type=type(self).__name__,
    #         msg=self.detail
    #     )

    def __str__(self):
        return "{msg}".format(
            msg=self.detail
        )

    def get_dict(self):
        error = {
            'status': job_status.FAILURE,
            'message': self.detail,
        }
        return error


class ValidationError(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = 'Value was invalid'


class ParseError(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = 'Malformed request.'


class AuthenticationFailed(APIException):
    """ Authentication is missing or incorrect """
    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = 'Incorrect authentication credentials.'


class PermissionDenied(APIException):
    """
    Authentication has succeeded, but user can not access resource.
    Re-authenticating will not fix this problem
    """
    status_code = status.HTTP_403_FORBIDDEN
    default_detail = 'You do not have permission to perform this action.'


class NotFound(APIException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = 'Not found.'


class MethodNotAllowed(APIException):
    status_code = status.HTTP_405_METHOD_NOT_ALLOWED
    default_detail = "Method '{}' not allowed."

    def __init__(self, method):
        self.detail = self.default_detail.format(method)


###############################################################################
#                                                                             #
#                              Specific Errors                                #
#                                                                             #
###############################################################################

class MissingArgumentError(ValidationError):
    default_detail = 'Missing argument: {}'

    def __init__(self, arg):
        self.detail = self.default_detail.format(arg)


class MissingKeyError(ValidationError):
    default_detail = 'Key not found: {}'

    def __init__(self, key):
        self.detail = self.default_detail.format(key)


class JobFailedError(APIException):
    default_detail = 'Job failed'
    status_code = status.HTTP_400_BAD_REQUEST

    def __init__(self, detail=None, error=None):
        if detail is not None:
            self.detail = force_text(detail)
        else:
            self.detail = force_text(self.default_detail)

        self.error = error

    def get_dict(self):
        error = {
            'status': job_status.FAILURE,
            'message': self.detail,
        }
        if self.error:
            error['error'] = self.error

        return error


class JobInProgress(APIException):
    status_code = status.HTTP_200_OK
    default_detail = 'Job in progress'

    def get_dict(self):
        error = {
            'status': job_status.RUNNING,
            'message': self.detail,
        }
        return error


class JSONError(ValidationError):
    default_detail = 'Invalid JSON'


class ConnectionError(APIException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    default_detail = 'Connection to micro-service failed: {}'

    def __init__(self, key, error):
        self.detail = self.default_detail.format(key)
        self.error = error


class HTTPCommunicationError(APIException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    default_detail = 'Connection to TerachemCloud failed: {}'

    def __init__(self, key, error):
        self.detail = self.default_detail.format(key)
        self.error = error

