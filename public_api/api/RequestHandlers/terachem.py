import logging
import json
import requests

from base import BaseRH
from api.exceptions import *
from api.settings import config

logger = logging.getLogger(__name__)


class TerachemRH(BaseRH):
    """
    Class to handle Terachem requests
    Takes a config object (dict in json) and a geometry,
    returns electronic structure data
    """

    SUPPORTED_METHODS = ('POST')

    def post(self):

        # Bugfix for backwards compatiblity with pytc<=1.5.1
        if 'tc_config' in self.json_args.keys():
            self.json_args['config'] = self.json_args['tc_config']

        # Sanity check
        required_keys = ['user_id', 'api_key', 'config', 'geom']
        valid, reason = self.check_json_keys(required_keys)
        if not valid:
            response = {'message': reason}
            self.set_status(400)
            self.write(response)
            return

        # request passes basic checks, send to real terachem cloud spi
        payload = self.json_args

        # Send HTTP request to tc-cloud
        url = config['tcc_url'] + ':' + str(config['tcc_port']) + config['submit_endpoint']
        try:
            r = requests.post(url, json=payload)
        except requests.exceptions.RequestException as e:
            raise HTTPCommunicationError('while sending POST for job submission', e)

        if r.status_code != requests.codes.ok:
            raise ServerError(r)

        # tc-cloud API call was okay, return results
        response = json.loads(r.text)
        self.set_status(200)
        self.write(r.text)

