import logging
import json

from tornado.web import RequestHandler
from tornado.options import options

logger = logging.getLogger(__name__)


class BaseRH(RequestHandler):
    """
    Base class for requests, handles mostly user authentication
    """
    def initialize(self):
        self.options = options.as_dict()

    def prepare(self):
        if self.request.headers["Content-Type"].startswith("application/json"):
            self.json_args = json.loads(self.request.body)
        else:
            self.json_args = None

    def check_json_keys(self, required_keys):
        if not self.json_args:
            missing_keys = required_keys
        else:
            missing_keys = [k for k in required_keys if k not in self.json_args.keys()]

        if len(missing_keys):
            if len(missing_keys) > 1:
                key_str = ','.join(missing_keys)
            else:
                key_str = missing_keys[0]

            return False, 'Error: {} not specified but required'.format(key_str)

        return True, None

    # def authenticate_user(self, user_id, api_key):
    #     user_exists = self.user_manager.user_exists(user_id)
    #     if not user_exists:
    #         return False, 'Error: user "{}" does not exist'.format(user_id)

    #     api_key_okay = self.user_manager.validate_api_key(user_id, api_key)
    #     if not api_key_okay:
    #         return False, 'Error: invalid API key "{}" for user "{}"'.format(api_key, user_id)

    #     return True, 'Authenticated user "{}"'.format(user_id)

    # def check_limits(self, user_id):
    #     limits = self.limits_manager.get_user_limits(user_id)
    #     job_count = self.limits_manager.get_user_job_count(user_id)

    #     if job_count > limits['job_limit']:
    #         msg = 'Error: user {} has {} jobs running, limit is {}'.format(user_id, job_count, limits['job_limit'])
    #         return False, msg

    #     else:
    #         return True, None
