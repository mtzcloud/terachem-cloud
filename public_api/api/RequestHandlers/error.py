import json
from tornado.web import RequestHandler, HTTPError


class MissingRH(RequestHandler):

    def prepare(self):
        response = {
            "status": "ERROR",
            "message": "The requested API endpoint does not exist",
        }
        self.msg = json.dumps(response)

    def get(self):
        self.write_error(status_code=404, message=self.msg)

    def post(self):
        self.write_error(status_code=404, message=self.msg)


class RootRH(RequestHandler):

    def prepare(self):
        response = {
            "status": "SUCCESS",
            "message": "Welcome to the ChemPix API server. There are two endpoints: /v1/image for receiving image files, and /v1/smiles for receiving smiles strings."
        }
        self.set_status(200)
        self.data = json.dumps(response)

    def get(self):
        self.write(self.data)

    def post(self):
        self.write(self.data)
