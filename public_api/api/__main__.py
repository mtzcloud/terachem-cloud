# Terachem public facing API module
# Execute with python -m api from the directory above
import logging

import tornado.ioloop
import tornado.web
import tornado.autoreload
from tornado.options import define, options

import RequestHandlers as rh


def get_app(settings=None):

    if settings is None:
        settings = {}

    application = tornado.web.Application([

        # receive geometry, return energy and forces
        (r"/v1/terachem/?", rh.TerachemRH),

        # retrieve results
        (r"/v1/job/?", rh.ResultDetailsRH),

        # Root route
        (r"/(?:v1)?/?", rh.RootRH),

        # Custom 404 error page
        (r".*", rh.MissingRH)


    ], settings)

    application.sentry_client = None

    return application


define(
    "dev",
    default=True,
    help=(
        "development mode: set to False disable. When enabled, Tornado will "
        "auto-reload on file change and will be in debug mode, and "
        "external monitoring will be disabled"
    )
)


def main():

    define("port", default=8888, help="listen on this port for HTTP connections")
    options.parse_command_line()

    settings = {}

    if options.dev:
        logging.info("Starting in debug mode")
        settings["debug"] = True
        settings["autoreload"] = True
        settings["enable_monitoring"] = False

    application = get_app(settings=settings)

    if settings["autoreload"]:
        tornado.autoreload.start()

    application.listen(options.port)
    logging.info("Starting tornado server on port {}".format(options.port))
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                        datefmt='%Y-%m-%d:%H:%M:%S',
                        level=logging.INFO)

    logger = logging.getLogger(__name__)
    main()
