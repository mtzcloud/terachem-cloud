# RECEIVED -> SUBMITTED -> RUNNING
# PENDING is not implemented yet, will be used for user limit queues
# SUCCESS or FAILURE

RECEIVED = 'RECEIVED'
SUBMITTED = 'SUBMITTED'
PENDING = 'PENDING'
RUNNING = 'RUNNING'

SUCCESS = 'SUCCESS'
FAILURE = 'FAILURE'
