# Public API for Terachem #

Simple web server to pass HTTP messages through to TeraChem Cloud

The server has two endpoints

* /v1/image - POST a .png of a molecule to this endpoint and a neural net will translate it into a SMILES string and return a rendered image plus the smiles string
* /v1/smiles - POST a valid SMILES string to this endpoint and receive back chemical data