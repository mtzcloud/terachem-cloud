# TeraChem in the Cloud #

REST API for TeraChem. 

This is a dockerized web server and celerefied job queue for the TeraChem _ab initio_ calculation engine.
Docker images are kept in Stefan's Docker Hub account (`sseritan`).
Legacy docker images for pre-Kubernetes deployment are kept in Keiran's Docker Hub account (`drenerbas`).

### How do I set up a GPU-capable Kubernetes cluster? ###

These instructions are based heavily on `https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm`, but adjusted specifically for Fire.
It is also important to look at the instructions for GPU scheduling with Kubernetes on `https://kubernetes.io/docs/tasks/manage-gpus/scheduling-gpus/`.

These instructions are for Kubernetes 1.13.0 (tested on 1.14.0 on AWS and up to 1.18.0 on an HPC cluster). Note that earlier versions of Kubernetes have a signficant security flaw (CVE-2018-1002105), are difficult to install on CentOS 7 (<1.11.0), or only have experimental GPU support (<1.10.0).

* Install `kubeadm, kubectl, kubelet` on all nodes that will be part of the Kubernetes cluster

* Run `sysctl net.bridge.bridge-nf-call-iptables=1` to ensure the `kube-router` extension will work properly

* Run `swapoff -a` to turn swap off for Kubernetes

* On GPU nodes, make the `nvidia` runtime default by editing `/etc/docker/daemon.json` and adding `"default-runtime": "nvidia",`, and then doing `systemctl restart docker`

* Start the master node with 
```
kubeadm init --pod-network-cidr=192.168.0.0/16 --kubernetes-version=1.13.0
```

    * Copy the last line of the output, which should contain the token and hash needed for joining the network

At this point, the master node should be up. 

Any user that wants access needs the cerification keys. This can be done by root for any account (just change `~` and user:group in `chown`) by
```
mkdir ~/.kube
cp -i /etc/kubernetes/admin.conf ~/.kube/config
chown root:root ~/.kube/config
export KUBECONFIG=~/.kube/config
```

The master node and access can be tested by running `kubectl cluster-info`.
You should have `Kubernetes master` and `KubeDNS` entries.

* Install the `kube-router` plugin:
```
kubectl apply -f https://raw.githubusercontent.com/cloudnativelabs/kube-router/master/daemonset/kubeadm-kuberouter.yaml
```

    * After installation, `kube-dns` should be up (can check with `kubectl get pods --all-namespaces`)

* Install the `nvidia-device` plugin:
```
kubectl create -f https://raw.githubusercontent.com/NVIDIA/k8s-device-plugin/1.0.0-beta4/nvidia-device-plugin.yml
```

* Remove the restriction of no pods on the master node by running `kubectl taint nodes --all node-role.kubernetes.io/master-`

* Add nodes to the cluster with the `kubeadm join` line from the `kubeadm init` line earlier. Remember to make all the changes to `kubelet.service` from install above!

* Pods are assigned to nodes using labels. Label CPU/frontend nodes using `kubectl label nodes <node-name> tcc_role=frontend` and all GPU/backend nodes using `kubectl label nodes <node-name> tcc_role=backend`.

Now, running `kubectl get nodes` should show all nodes in your cluster.
Congratulations, a Kubernetes cluster is now live!

To add another node after the first token expires, use
```
kubeadm token create --print-join-command
```
on the master node.

To tear down a cluster, follow these instructions:
```
kubectl drain <node name> --delete-local-data --force --ignore-daemonsets
kubectl delete node <node name>
kubeadm reset
```

Be extremely careful with `kubeadm reset`.
Treat it with the same respect you give `sudo rm -rf *`,
because it will completely wipe whatever Kubernetes cluster setup you have configured.

You may need to clean up the networking interfaces on nodes by hand:
```
iptables -F && iptables -t nat -F && iptables -t mangle -F && iptables -X
ip link set kube-bridge down
brctl delbr kube-bridge
```

### How do I deploy TeraChem Cloud on a Kubernetes cluster? ###

On Fire, I have provided convenience scripts in `deploy_k8s.sh` and `teardown_k8s.sh`.

However, in case more advanced deployment is necessary,
the basic Kubernetes configurations are listed in `deployment/*.yaml`.
You simply need to run `kubectl create -f <component>.yaml` in order to launch each component.

ORDER MATTERS. Make sure to launch the midlayer (`rabbitmq` and `redis-*`) before the frontend (`api-server`)
and the volumes (`persistent-volume`, `persistent-volume-claims`, and the license `ConfigMap`) before the backend workers.
Also, start Services before Deployments as a general rule.

To take down TeraChem Cloud, just call `kubectl delete` on all components you launched with `kubectl create`.

#### tcpb-server Docker Image ####

Since the `tcpb-server` image is private, you will need Docker privileges to pull that on every node where the `terachem-worker` Pod will be deployed.
This is best handled using a Docker credential secret, which is described at `https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/`.

To create the Docker secret:

```
kubectl create secret docker-registry regcred --docker-server=https://index.docker.io/v1/ --docker-username=sseritan --docker-password=REDACTED --docker-email=sseritan@gmail.com
```

### How do I tell it's working? ###

* Go to the repo _tcc-python_, install the TCC client, and run `python examples/simple-example.py` to submit a job! :)

### Basic Anatomy of Kubernetes Deployment ###

Groups of containers are called Pods - 
most TeraChem Cloud containers are in their own Pods, with the expection of worker pods.
Each Pod is assigned an "cluster IP", and containers within Pods can communicate freely with `localhost`.

Containers can be exposed to other Pods or externally with Services - 
most TeraChem Cloud Pods in the frontend or midlayer have associated Services.
Services make it easier for containers to find each other by creating DNS entries,
so that Pod cluster IPs can be resolved dynamically by container name.
This is what makes it possible (and easy!) to have containers on multiple nodes (or even clusters) communicating.

When Pods die, they are not restarted by default.
Kubernetes revival mechanism are called ReplicaSets, which also control scaling the number of Pod instances.
A Deployment is a Replica Set built on top of a Pod template.
All TeraChem Cloud containers are wrapped in Deployments (more specifics below).

Similar to Docker, Kubernetes also provides a lot of options for mounting directories and files through Volumes.
There are two stages to Volumes: PersistentVolumes define the available resources, and PersistentVolumeClaims actually reserve the resources.
Kubernetes Volumes are a lot more diverse than Docker volumes - while `hostPath` is an option,
you can also directly mount NFS and some other cloud storage solutions from Google Cloud Environment (GCE) and Amazon Web Services (AWS).

The last piece of the puzzle are ConfigMaps and Secrets.
These let you reconfigure containers or provide sensitive information at launch time rather than container build time.
In particular, TeraChem Cloud uses a ConfigMap to provide the `tcpb-server` container with a valid TeraChem license.
It probably should be Secret, but the TeraChem `license.key` file is more amenable to a ConfigMap.

### Debugging Kubernetes ###

Assuming you have a working Kubernetes cluster on top of nvidia-docker2, you don't need to touch Docker directly anymore.
Here are a few handy commands for debugging Kubernetes:

* `kubectl get deploy,svc,pv,pvc,configmap`: Show all active components of TeraChem Cloud. Replaces `docker ls`.

* `kubectl get pods`: Show all pods, which is necessary to find logfiles

* `kubectl describe <resource> <name>`: Get details for a certain resource (e.g. `deploy`, `service`, etc.). Replaces `docker inspect`.

* `kubectl logs <pod> <container> | less`: Dump logs for a certain container within a given Pod. Replaces `docker logs`.

* `kubectl scale --replicas=<#> deploy/<deployment>`: Scale the number of replicas in a Deployment. Makes all of Kubernetes worth it.

While comparing Kubernetes and Docker, it's also worth mentioning that `kubectl create` is like `docker run -d`
and `kubectl delete` is like `docker rm && docker kill`.

### Debugging Docker ###

A couple few handy commands for debugging Docker containers:

* `docker logs <container name>`: Print the stdout of the main process in each container

* `docker run -it <image name> /bin/bash`: General-purpose interactive Docker container (for those based on a Linux image)

* `NV_GPU='2,3' docker run -it -p 54321:54321 -v /u/tccloud:/scratch --name tcpb-server sseritan/tcpb-server terachem -s 54321`: Launches a TCPB container on GPUs 2 and 3 on port 54321 (with scratch mounted in the container)

* `docker exec -it <container name/id> /bin/bash`: Attach to a currently running container with a new `bash` shell (handy to find logfiles in running containers)


### Who do I talk to? ###

* Keiran Thompson  [keiran@stanford.edu](mailto:keiran@stanford.edu)
* Stefan Seritan  [sseritan@stanford.edu](mailto:sseritan@stanford.edu)
