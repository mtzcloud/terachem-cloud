import os
import sys
import hashlib
import json
import pprint
import requests
import argparse

COOKIE = '.cookie'


class AdminClient(object):
    """A user administration client for the TeraChem API server"""
    def __init__(self, magic_word, port=8888, host='http://localhost'):
        self.url = host + ":" + str(port)
        self.submit_endpoint = '/admin'
        self.body = {
            'user_id': 'admin',
            'magic_word': magic_word
        }

    def ping(self):
        self.body['action'] = 'ping'
        r = requests.post(self.url + self.submit_endpoint, json=self.body)
        result = json.loads(r.text)
        if r.status_code == 200 and result['message'] == 'pong':
            return True
        else:
            return False

    def post_request(self, payload):
        # Send HTTP request
        # print 'payload:', payload
        r = requests.post(self.url + self.submit_endpoint, json=payload)
        print r.status_code
        result = json.loads(r.text)
        print str(result['message'])
        del result['message']
        if result is not None:
            for k, v in result.iteritems():
                print k, pprint.pformat(v)

    def add_user(self, user_name):
        self.body['target_user'] = user_name
        self.body['action'] = 'create_user'
        self.post_request(self.body)

    def delete_user(self, user_name):
        self.body['target_user'] = user_name
        self.body['action'] = 'delete_user'
        self.post_request(self.body)

    def new_api_key(self, user_name):
        self.body['target_user'] = user_name
        self.body['action'] = 'new_api_key'
        self.post_request(self.body)

    def list_user(self, user_name):
        self.body['target_user'] = user_name
        self.body['action'] = 'list_user'
        self.post_request(self.body)

    def list_users(self):
        self.body['action'] = 'list_users'
        self.post_request(self.body)

    def adjust_user_limits(self, user_name, limit, value):
        self.body['target_user'] = user_name,
        self.body['action'] = 'adjust_limits'
        self.body['target_limit'] = limit
        self.body['new_value'] = value
        self.post_request(self.body)


def run(params):

    # -----------------------------------------------
    #  admin authentication with the API server
    # -----------------------------------------------
    # idea is to authenticate once only
    # look for local cookie like thing
    if os.path.exists(COOKIE):
        with open(COOKIE, 'r') as f:
            magic_word = f.read()
    # otherwise get password from the command line, hash it and save it
    else:
        plain_text = raw_input('Enter the magic word: ')
        magic_word = hashlib.sha256(plain_text).hexdigest()
        with open(COOKIE, 'w') as f:
            f.write(magic_word)

    # create a client instance and ping it, if the password is bad, remove the cookie
    client = AdminClient(magic_word=magic_word, host=params.host, port=params.port)
    pong = client.ping()
    if not pong:
        print('Pong for admin login failed')
        os.remove(COOKIE)
        sys.exit(1)

    # -----------------------------------------------

    if params.add_user is not None:
        client.add_user(params.add_user)

    elif params.delete_user is not None:
        client.delete_user(params.delete_user)

    elif params.new_api_key is not None:
        client.new_api_key(params.new_api_key)

    elif params.user_info is not None:
        client.list_user(params.user_info)

    elif params.list is not None:
        client.list_users()

    elif params.change_limits is not None:
        if params.set_job_limit is not None:
            limit = 'job_limit'
            value = params.set_job_limit
            client.adjust_user_limits(params.change_limits, limit, value)
        if params.set_rate_limit is not None:
            limit = 'rate_limit'
            value = params.set_rate_limit
            client.adjust_user_limits(params.change_limits, limit, value)
        if params.set_usage_limit is not None:
            limit = 'usage_limit'
            value = params.set_usage_limit
            client.adjust_user_limits(params.change_limits, limit, value)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--host', type=str, default='http://localhost', help='url for TeraChemCloud API', required=True)
    parser.add_argument('--port', type=str, default='8888', help='port for TeraChemCloud API', required=True)
    parser.add_argument('--add_user', type=str, help='create a new user')
    parser.add_argument('--user_name', type=str, help='name of user to add or modify')
    parser.add_argument('--delete_user', type=str, help='delete a user from the TeraChemCloud API')
    parser.add_argument('--new_api_key', type=str, help='generate a new API_KEY for the named user')
    parser.add_argument('--change_limits', type=str, help='change limits for a user')
    parser.add_argument('--user_info', type=str, help='list details of a particular user')
    parser.add_argument('--list', help='return a list of all users', action='store_true')
    parser.add_argument('--set_job_limit', type=int, default='10', help='change the number of simultaneous jobs allowed')
    parser.add_argument('--set_rate_limit', type=int, default='1000', help='change the number of jobs allowed in a given 24 hour period')
    parser.add_argument('--set_usage_limit', type=int, default='10000', help='change the total resource usage limit')

    params, unparsed = parser.parse_known_args()
    # print params
    if len(unparsed) > 0:
        print 'unrecognised command line arguments:', ','.join(unparsed)

    run(params)

