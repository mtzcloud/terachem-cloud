# User admin client for TC Cloud #

This is a tool for managing user accounts and resource limits on the Terachem Cloud API server. 

## Usage ##

__tccloud_admin__ is a command line python script which uses the requests library to commincate with the API server.

#### Adding a new user ####

Try this:
```bash
python tccloud_admin --add_user ghost
```

You should see something like:
```
200
user 'ghost' created
API_KEY u'$2a$12$QWLJxwpDieToHEmN96/tYOtN9FebkzvoPG/pCIPmv1eMH8jXVwIHu'
```

Keep your API_KEY somewhere safe, you will need to include it in every call to the Terachem Cloud API.

#### Deleting user ####

Try this:
```bash
python tccloud_admin --delete_user ghost
```

You should see something like:
```
200
user 'ghost' deleted
```





### Security Considerations ###

This tool is not intended to be particularly secure. The admin client connects to the `/admin` endpoint and authenticates with plain text string which is known to both pieces of source code. Not even security by obscurity :)

### Who do I talk to? ###

* Keiran Thompson  [keiran@stanford.edu](mailto:keiran@stanford.edu)
* Stefan Seritan  [sseritan@stanford.edu](mailto:sseritan@stanford.edu)
