#!/bin/bash

# Simple script to deploy TeraChem Cloud through Kubernetes
# ORDER MATTERS

# Docker regcred secret must be created manually
#kubectl create secret docker-registry regcred --docker-server=https://index.docker.io/v1/ --docker-username=sseritan --docker-password=REDACTED --docker-email=sseritan@gmail.com

# Create persistent volumes for Fire
kubectl apply -f data-pv.yaml -f data-pvc.yaml 

# Create TeraChem license Secret
kubectl create secret generic terachem-license --from-file=/global/software/TeraChem/2018.07-intel-2016.0.109-MPICH2-1.4.1p1/license.key

# Create Celery config ConfigMap
kubectl create configmap celery-config --from-file=./config.yaml
kubectl create configmap tc-config --from-file=../celery/api/validation/tc_options.yaml

# Create RabbitMQ/REDIS deployment and service
kubectl apply -f rabbitmq-svc.yaml -f rabbitmq-deploy.yaml
kubectl apply -f redis-celery-svc.yaml -f redis-celery-deploy.yaml
kubectl apply -f redis-api-svc.yaml -f redis-api-deploy.yaml

# Create Tornado/Flower deployment and service
kubectl apply -f api-server-svc.yaml -f api-server-deploy.yaml
kubectl apply -f flower-svc.yaml -f flower-deploy.yaml

# Create many replicas of the Celery Worker/TCPB Server pods
kubectl apply -f terachem-worker-deploy.yaml

# Display deployments and services
sleep 1
kubectl get deploy,svc,pv,pvc,configmap
