#!/bin/bash

# Simple script to completely tear down a Kubernetes TeraChem Cloud
# Order kinda matters, but much less than deployment

# Remove Tornado web server
kubectl delete deployment,service api-server
kubectl delete deployment,service celery-flower

# Remove TCPB Workers
kubectl delete deploy terachem-worker

# Remove RabbitMQ/REDIS Service and Deployment
kubectl delete deployment,service redis-api
kubectl delete deployment,service redis-celery
kubectl delete deployment,service rabbitmq

# Remove persistent volumes and configmap
kubectl delete pvc data
kubectl delete pv data
kubectl delete configmap terachem-license celery-config tc-config

# Display for double checking
kubectl get deploy,svc,pv,pvc,configmap
